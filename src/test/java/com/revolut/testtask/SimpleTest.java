package com.revolut.testtask;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.concurrent.*;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class SimpleTest {
    private TestTaskServer server = new TestTaskServer();
    private CloseableHttpClient httpClient = HttpClients.createDefault();

    @Before
    public void init() throws Exception {
        server.start();
    }

    @After
    public void destroy() throws Exception {
        server.stop();
    }

    @Test
    public void simpleTest() throws IOException, URISyntaxException {
        String first = createNewAccount();
        String second = createNewAccount();

        addMoney(first, 1000);
        addMoney(second, 2000);

        Assert.assertTrue(getAmount(first) == 1000);
        Assert.assertTrue(getAmount(second) == 2000);

        transfer(first, second, 500);

        Assert.assertTrue(getAmount(first) == 500);
        Assert.assertTrue(getAmount(second) == 2500);
    }

    @Test
    public void concurrencyTest() throws IOException, URISyntaxException {
        String first = createNewAccount();
        String second = createNewAccount();

        ExecutorService executor = Executors.newFixedThreadPool(8);

        for (int i = 0; i < 10000; ++i) {
            executor.execute(() -> {
                try {
                    addMoney(first, 1);
                } catch (IOException | URISyntaxException e) {
                    Assert.assertTrue(false);
                }
            });
        }

        for (int i = 0; i < 20000; ++i) {
            executor.execute(() -> {
                try {
                    addMoney(second, 1);
                } catch (IOException | URISyntaxException e) {
                    Assert.assertTrue(false);
                }
            });
        }

        for (int i = 0; i < 10000; ++i) {
            executor.execute(() -> {
                try {
                    transfer(first, second, 1);
                } catch (IOException | URISyntaxException e) {
                    Assert.assertTrue(false);
                }
            });
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException ignored) {
        }

        Assert.assertTrue(getAmount(first) == 0);
        Assert.assertTrue(getAmount(second) == 30000);
    }

    private String createNewAccount() throws IOException {
        HttpGet httpGet = new HttpGet("http://localhost:8080/create");

        String id;
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();

            id = readFully(entity.getContent());
            Assert.assertFalse(id.isEmpty());

            EntityUtils.consume(entity);
        }

        return id;
    }

    private void transfer(String from, String to, long amount) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost").setPort(8080).setPath("/transfer")
                .setParameter("from", from)
                .setParameter("to", to)
                .setParameter("amount", "" + amount);

        HttpGet httpGet = new HttpGet(builder.build());

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();

            int code = response.getStatusLine().getStatusCode();
            Assert.assertTrue(200 <= code && code < 300);

            EntityUtils.consume(entity);
        }
    }

    private long getAmount(String id) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost").setPort(8080).setPath("/check")
                .setParameter("id", id);

        HttpGet httpGet = new HttpGet(builder.build());
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();

            long amount = Long.parseLong(readFully(entity.getContent()));
            Assert.assertTrue(amount >= 0);

            EntityUtils.consume(entity);

            return amount;
        }
    }

    private long addMoney(String id, long amount) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost").setPort(8080).setPath("/add")
                .setParameter("id", id)
                .setParameter("amount", "" + amount);

        HttpGet httpGet = new HttpGet(builder.build());
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();

            int code = response.getStatusLine().getStatusCode();
            Assert.assertTrue(200 <= code && code < 300);

            EntityUtils.consume(entity);

            return amount;
        }
    }

    private static String readFully(InputStream in) throws IOException {
        StringBuilder builder = new StringBuilder();
        byte[] buffer = new byte[1024];

        int length = in.read(buffer);
        while (length != -1) {
            builder.append(new String(buffer, 0, length));
            length = in.read(buffer);
        }

        return builder.toString();
    }
}
