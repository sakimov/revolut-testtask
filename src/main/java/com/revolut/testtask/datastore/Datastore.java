package com.revolut.testtask.datastore;

import com.revolut.testtask.model.Account;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class Datastore {
    private static Map<String, Account> accounts = new ConcurrentHashMap<>();

    public static boolean add(Account account) {
        return accounts.putIfAbsent(account.getId(), account) == null;
    }

    public static Account findById(String id) {
        return accounts.get(id);
    }
}
