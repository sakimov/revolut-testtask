package com.revolut.testtask.servlet;

import com.revolut.testtask.datastore.Datastore;
import com.revolut.testtask.model.Account;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class AddMoneyServlet extends HttpServlet {
    private static final String ID_PARAMETER = "id";
    private static final String AMOUNT_PARAMETER = "amount";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter(ID_PARAMETER);
        long amount = Long.parseLong(request.getParameter(AMOUNT_PARAMETER));

        Account account = Datastore.findById(id);

        if (account == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getOutputStream().print("Can't find account");
        } else {
            synchronized (account) {
                account.add(amount);
            }
            response.setStatus(HttpServletResponse.SC_OK);
            response.getOutputStream().print("New balance: " + account.getAmount());
        }

        response.getOutputStream().close();
    }
}
