package com.revolut.testtask.servlet;

import com.revolut.testtask.datastore.Datastore;
import com.revolut.testtask.model.Account;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class MoneyTransferServlet extends HttpServlet {
    private static final String FROM_PARAMETER = "from";
    private static final String TO_PARAMETER = "to";
    private static final String AMOUNT_PARAMETER = "amount";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String from = request.getParameter(FROM_PARAMETER);
        String to = request.getParameter(TO_PARAMETER);
        long amount = Long.parseLong(request.getParameter(AMOUNT_PARAMETER));

        try {
            doTransfer(from, to, amount);
            //doTransferLockOrdering(from, to, amount);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.getOutputStream().print(e.getMessage());
            response.getOutputStream().close();
            return;
        }

        response.setStatus(HttpServletResponse.SC_OK);
        response.getOutputStream().print("OK");
        response.getOutputStream().close();
    }

    private void doTransfer(String fromId, String toId, long amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Can't transfer negative amount");
        }

        Account from = Datastore.findById(fromId);
        Account to = Datastore.findById(toId);

        //I do understand that you probably want me to implement something like lock-ordering but I don't see the case why this implementation is wrong.
        // I would really appreciate a test-case which fails it
        // Anyway I provide 'classic' implementation below
        synchronized (from) {
            from.withdraw(amount);
        }
        synchronized (to) {
            to.add(amount);
        }
    }

    private static final Object lock = new Object();
    private void doTransferLockOrdering(String fromId, String toId, long amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Can't transfer negative amount");
        }

        Account from = Datastore.findById(fromId);
        Account to = Datastore.findById(toId);

        class Helper {
            public void transfer() {
                from.withdraw(amount);
                to.add(amount);
            }
        }

        int fromHash = System.identityHashCode(from);
        int toHash = System.identityHashCode(to);

        if (fromHash < toHash) {
            synchronized (from) {
                synchronized (to) {
                    new Helper().transfer();
                }
            }
        } else if (fromHash > toHash) {
            synchronized (to) {
                synchronized (from) {
                    new Helper().transfer();
                }
            }
        } else {
            synchronized (lock) {
                synchronized (from) {
                    synchronized (to) {
                        new Helper().transfer();
                    }
                }
            }
        }
    }
}
