package com.revolut.testtask.servlet;

import com.revolut.testtask.datastore.Datastore;
import com.revolut.testtask.model.Account;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class CreateAccountServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        Account account = Account.newAccount();

        boolean success = Datastore.add(account);

        if (success) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getOutputStream().print(account.getId());
        } else {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            response.getOutputStream().print("You are extremely unlucky.");
        }

        response.getOutputStream().close();
    }
}
