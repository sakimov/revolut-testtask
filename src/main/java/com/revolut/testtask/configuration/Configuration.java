package com.revolut.testtask.configuration;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class Configuration {
    private static Properties properties = new Properties();

    static {
        try {
            properties.load(Configuration.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read configuration", e);
        }
    }

    public static int getPort() {
        return Integer.parseInt(properties.getProperty("port", "8080"));
    }

    public static int getMaxIdleTime() {
        return Integer.parseInt(properties.getProperty("max.idle.time", "45000"));
    }

    public static String getAccessLogFile() {
        return properties.getProperty("access.log.file", "access.log");
    }

}
