package com.revolut.testtask.model;

import java.util.UUID;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class Account {
    private final String id;
    private long amount = 0;

    public static Account newAccount() {
        String id = UUID.randomUUID().toString();

        return new Account(id);
    }

    public Account(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public synchronized long getAmount() {
        return amount;
    }

    public void add(long amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Can't add negative amount.");
        }

        this.amount += amount;
    }

    public void withdraw(long amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Can't withdraw negative amount.");
        }

        if (this.amount  < amount) {
            throw new IllegalArgumentException("Account has not enough money to withdraw");
        }

        this.amount -= amount;
    }
}
