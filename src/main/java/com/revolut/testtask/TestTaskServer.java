package com.revolut.testtask;

import com.revolut.testtask.configuration.Configuration;
import com.revolut.testtask.servlet.AddMoneyServlet;
import com.revolut.testtask.servlet.CheckAccountServlet;
import com.revolut.testtask.servlet.CreateAccountServlet;
import com.revolut.testtask.servlet.MoneyTransferServlet;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class TestTaskServer {

    private Server server;

    public void start() throws Exception {
        server = new Server();

        ServerConnector httpConnector = new ServerConnector(server);
        httpConnector.setPort(Configuration.getPort());
        httpConnector.setReuseAddress(true);
        httpConnector.setIdleTimeout(Configuration.getMaxIdleTime());

        server.setConnectors(new Connector[]{httpConnector});

        server.setHandler(createHandlers());
        server.start();
    }

    public void stop() throws Exception {
        server.stop();
    }

    private static HandlerCollection createHandlers() {
        Handler[] handlerArray = {createRequestLogHandler(), createServletContextHandler(), new DefaultHandler()};

        HandlerCollection handlers = new HandlerCollection();
        handlers.setHandlers(handlerArray);
        return handlers;
    }

    private static Handler createServletContextHandler() {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/");

        context.addServlet(new ServletHolder(new MoneyTransferServlet()), "/transfer");
        context.addServlet(new ServletHolder(new CreateAccountServlet()), "/create");
        context.addServlet(new ServletHolder(new CheckAccountServlet()), "/check");
        context.addServlet(new ServletHolder(new AddMoneyServlet()), "/add");

        return context;
    }

    private static RequestLogHandler createRequestLogHandler() {
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        NCSARequestLog requestLog = new NCSARequestLog(Configuration.getAccessLogFile());
        requestLog.setRetainDays(2);
        requestLog.setAppend(true);
        requestLog.setExtended(true);
        requestLogHandler.setRequestLog(requestLog);

        return requestLogHandler;
    }
}
