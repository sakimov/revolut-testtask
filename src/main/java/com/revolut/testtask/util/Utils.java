package com.revolut.testtask.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Sergey Akimov (akimovsa@gmail.com)
 */
public class Utils {
    public static String readFully(InputStream in) throws IOException {
        StringBuilder builder = new StringBuilder();
        byte[] buffer = new byte[1024];

        int length = in.read(buffer);
        while (length != -1) {
            builder.append(new String(buffer, 0, length));
            length = in.read(buffer);
        }

        return builder.toString();
    }
}
