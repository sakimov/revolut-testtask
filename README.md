## Requirements: ##
java 1.8 and above

## Build: ##
mvn package

## Usage: ##
java -jar jetty-server.jar

## Test: ##
Please see Tests for usage.

/create   - new account

/add      - adds money to specified account

/check    - check balance

/transfer - transfer money from account A to account B